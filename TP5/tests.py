#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 01:43:16 2021

@author: asus
"""
import unittest
from linkedList import LinkedList, Node


class TestLinkedList(unittest.TestCase):
    """ Samples of tests which could be made on a chained list """
    def setUp(self):
        """ Sets a list on which we will work on for the next steps """
        self.linked_list = LinkedList(["K", "A", "O", "U", "T", "H", "E", "R"])
    def test_linkedlist_is_empty(self):
        """ tests if the chained list has no elements """
        liste = LinkedList([])
        self.assertIsNone(liste.head, msg="Not an empty list !")
    def test_l_inkedlist_not_empty(self):
        """ tests if the chained list has at least an element"""
        liste = LinkedList(['hey'])
        self.assertIsNotNone(liste.head, msg="It's an empty list !")
    def test_linkedlist_length_same(self):
        """ tests if the length of the linked list is the same after an
        addition and a remove of a node """
        len_primary_list = self.linked_list.length()
        self.linked_list.add_last(Node(' IS THE BEST'))
        #print(self.linked_list)
        self.linked_list.remove_node(' IS THE BEST')
        #print(self.linked_list)
        len_final_list = self.linked_list.length()
        self.assertEqual(len_primary_list, len_final_list, msg="Not the same")
    def test_linkedlist_elements_same(self):
        """ tests if the elements of a linked list are the same after an
        addition and a remove of a node """
        copy1 = LinkedList(["K", "A", "O", "U", "T", "H", "E", "R"])
        #print(copy1)
        #self.linked_list.remove_node('O')
        #self.linked_list.add_last(Node('R'))
        #print(self.linked_list)
        #print(self.linked_list.are_identical(copy1))
        self.assertEqual(True, self.linked_list.are_identical(copy1), msg="#elts")
    def test_four(self):
        """Tests if the added node is on the top of the linkedlist"""
        check = Node('PERFECT')
        self.linked_list.add_last(check)
        #print(self.linked_list.last_element())
        print(self.linked_list)
        self.assertEqual(self.linked_list.last_element(), check)

if __name__ == "__main__":
    unittest.main()
