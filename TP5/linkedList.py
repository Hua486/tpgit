# coding: utf-8
class Node:
    """ Node of a list """
    def __init__(self, data):
        self.data = data
        self.next = None
    def __repr__(self):
        return self.data
class LinkedList:
    """ Linked list """
    def __init__(self, nodes=None):
        self.head = None
        if nodes is not None and len(nodes) != 0:
            node = Node(data=nodes.pop(0))
            self.head = node
            for elem in nodes:
                node.next = Node(data=elem)
                node = node.next
    def get(self, index):
        if self.head is None:
            raise Exception("Node is empty")
        else:
            self.recurs(index, self.head)
    def recurs(self, index, node):
        print(index, node)
        if node is None:
            return index, node
        elif index == 0:
            return node
        else:
           return self.recurs(index -1, node.next)
    def length(self):
        length_chained_list = 0
        for node in self:
            if node.data is not None:
                length_chained_list += 1
        return length_chained_list
    def last_element(self):
        """ returns the last element of the linked_list"""
        current = self.head
        while current.next is not None:
            current = current.next
        return current
    def add_after(self, data, new_node):
        if not self.head:
            raise Exception("List is empty")

        for node in self:
            if node.data == data:
                new_node.next = node.next
                node.next = new_node
                return

        raise Exception("Node with data '{}' not found".format(data))

    def are_identical(self, listb):
        """ checks if the elements of 2 linkeds lists are the same """
        result = True
        first_list_e1 = self.head
        second_list_e1 = listb.head
        while (first_list_e1 is not None and second_list_e1 is not None):
            if (first_list_e1.data != second_list_e1.data):
                result = False
                print("Not same data ! ")
                break
            else:
                first_list_e1 = first_list_e1.next
                second_list_e1 = second_list_e1.next
        return result
    def add_before(self, data, new_node):
        if not self.head:
            raise Exception("List is empty")

        if self.head.data == data:
            return self.add_first(new_node)
        prev_node = self.head
        for node in self:
            if node.data == data:
                prev_node.next = new_node
                new_node.next = node
                return
            prev_node = node

        raise Exception("Node with data '{}' not found".format(data))

    def remove_node(self, data):
        if not self.head:
            raise Exception("List is empty")

        if self.head.data == data:
            self.head = self.head.next
            return

        previous_node = self.head
        for node in self:
            if node.data == data:
                previous_node.next = node.next
                return
            previous_node = node

        raise Exception("Node with data '{}' not found".format(data))



    def add_first(self, node_to_add):
        node_to_add.next = self.head
        self.head = node_to_add
    
    def add_last(self, node_to_add):
        if self.head == None:
            self.head = node_to_add
            return

        node = self.head
        # while node.next is not None:*
        while node.next is not None:
            node = node.next
        node.next = node_to_add

    def __repr__(self):
        node = self.head
        nodes = []
        while node is not None:
            nodes.append(node.data)
            node = node.next
        #return "a"
        return "{}".format(nodes)

    def __iter__(self):
        node = self.head
        while node is not None:
            yield node
            node = node.next

if __name__ == "__main__":
    llist = LinkedList(["a", "b", "c", "d", "e"])
    """ llist.add_last(Node("g"))
    llist.add_first(Node("z"))
    llist.add_before("z", Node("y"))
    llist.add_before("y", Node("x"))
    llist.add_before("g", Node("f"))
    llist.add_after("g", Node("h"))
    #llist.add_before("q", Node("r"))
    #llist.remove_node("q")
    llist.remove_node("a")
    print(llist)
    llist.remove_node("x")
    """
    #l =  LinkedList([])
    #l.get(0)
    print(llist.recurs(3, llist.head))
    print(llist)