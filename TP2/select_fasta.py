#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Its a code that reads the fasta file located in the data/fasta folder of
pymetacline package and select a user-defined set of sequences identifiers
(argument -seq that accept a comma separated list of sequence identifiers or a
set of identifiers using a positional argument).
@author: Kaouther Harbi
"""
import argparse
import re
from Bio import SeqIO

def create_parser():
    """This function creats our parser"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-i',
                        '--inputfile',
                        type=str,
                        required=True,
                        help="An input Fasta file is REQUIRED")
    # name output file
    parser.add_argument('-out',
                        '--outptfile',
                        type=str,
                        required=True,
                        help="A name for an output file is REQUIRED")
    # get list of identifiers of which we want the sequences
    parser.add_argument('-seq',
                        '--sequence',
                        type=str,
                        required=True,
                        help="At least one identifier is REQUIRED")
    return parser


def analyse_file(file_name, identifiers, file_out):
    """ This function extracts sequences from fasta file by their ids and puts
the result into a file"""
    seq = identifiers.split(',')
    records = list(SeqIO.parse(file_name, "fasta"))

    def fasta_out(rec):
        return ">" + str(rec.description) + "\n" + str(rec.seq)

    def formater(rec):
        return re.sub("[.][^ ]*$", "", rec.description)

    # Define the records to keep:
    keep = [fasta_out(i) for i in records if formater(i) in seq]
    # Write them out:

    file = open(file_out, 'w')
    # We can check:
    file.writelines("\n".join(keep))
    file.close()

#call whole functions
def main():
    """This function calls our main function called analyse file"""
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    print(args)
    analyse_file(args["inputfile"], args["sequence"], args["outptfile"])


if __name__ == "__main__":
    main()
