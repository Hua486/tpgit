#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 09:35:25 2021

@author: Kaouther Harbi
"""

# coding: utf-8
class TreeNode:
    def __init__(self: object, data):
        self.data = data
        self.right = None
        self.left = None
        self.level = 0
    def print_TreeNode(self):
        """print tree node right child first then the left"""
        if self.right:
            self.right.print_TreeNode()
        print(self.data)
        if self.left:
            self.left.print_TreeNode()
    def is_leaf(self):
        """ check if the node is a leaf """
        return self.right is None and self.left is None
    def __str__(self):
        """ print tree"""
        if self.is_leaf():
            return str(self.data)
        return "["+ str(self.left) +";"+ str(self.right) +"]:"+ str(self.data)
    def search(self, target_data):
        """ look for a specefic key in the tree"""
        if self.data == target_data:
            return self
        loop = None
        if self.left is not None:
            loop = self.left.search(target_data)
        if loop is None and self.right:
            loop = self.right.search(target_data)
        return loop
    def search_father(self, target_data):
        """ function to find father node of a target """
        if self.left is not None and self.left.data == target_data:
            return self
        if self.right is not None and self.right.data == target_data:
            return self
        #if the target wasn't found in the children we look in deeper children
        searched_data = None
        if self.left is not None:
            searched_data = self.left.search_father(target_data)
        if searched_data is None and self.right is not None:
            searched_data = self.right.search_father(target_data)
        return searched_data

class Tree:
    def __init__(self: object, rootNode):
        """constructor"""
        self.rootNode = rootNode
    def traversal_deep(self):
        """visulize tree"""
        print(self.rootNode)
    def search(self, target_data):
        """ look for a specefic key in the tree"""
        return self.rootNode.search(target_data)
    def insert_str(self, param_added_node_str, target_node_str):
        """insert node by giving its data as an str """
        node_find = self.search(target_node_str)
        self.insert(param_added_node_str, node_find)
    def insert_node(self, added_node, target_node):
        """ insert node"""
        if self.rootNode is  None:
            self.rootNode = added_node
            # If the added nod is less then the target node key
            # Assign the new node to be its left child
        elif not target_node.is_leaf():
            if target_node.left is None:
                target_node.left = added_node
            elif target_node.right is None:
                target_node.right = added_node
            else:
                self.insert_node(added_node, target_node.left)
        else:# target node is leaf
            target_node.right = added_node
    def insert(self, param_added_node_str, target_node):
        """add a new node"""
        added_node = TreeNode(param_added_node_str)
        if self.rootNode is  None:
            self.rootNode = added_node
            # If the added nod is less then the target node key
            # Assign the new node to be its left child
        elif not target_node.is_leaf():
            if target_node.left is None:
                target_node.left = added_node
            elif target_node.right is None:
                target_node.right = added_node
            else:
                self.insert(param_added_node_str, target_node.left)
        else:# target node is leaf
            target_node.right = added_node
    def remove_leaf(self, target_node):
        """delete a leaf"""
        if target_node is None:
            return
            # if the target node is a leaf it takes None as a value
        if target_node.is_leaf:
            target_node = None
    def delete_node(self, target_node_str):
        """ delete a node by giving a key which is a data """
        target_delete_node = self.rootNode.search(target_node_str)
        target_father_node = self.rootNode.search_father(target_node_str)
        if target_delete_node.left is not None:
            if target_father_node.left == target_delete_node:
                target_father_node.left = target_delete_node.left
            else:
                target_father_node.right = target_delete_node.left
            target_father_node.insert_node(target_delete_node.right, target_father_node)
        elif target_delete_node.right is not None:
            if target_father_node.left == target_delete_node:
                target_father_node.left = target_delete_node.left
            else:
                target_father_node.right = target_delete_node.left
        else:
            if target_father_node.left == target_delete_node:
                target_father_node.left = None
            else:
                target_father_node.right = None
if __name__ == "__main__":
    kaouther = TreeNode('kaouther')
    arbre = Tree(kaouther)
    print('\n Before \n')
    #n0.print_TreeNode()
    #arbre.traversal_deep()
    arbre.insert_str('l3', 'kaouther')
    print(kaouther)
    arbre.insert_str('l4', 'l3')
    arbre.insert_str('l5', 'kaouther')
    arbre.insert_str('l6', 'kaouther')
    print('\n After insert \n')
    print(kaouther)
    kaouther.print_TreeNode()
    print('\n')
    #print(arbre.search('l3'))
    arbre.delete_node('l6')
    print('\n After delete \n')
    kaouther.print_TreeNode()
    print(kaouther)
