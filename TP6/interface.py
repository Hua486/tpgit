#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 08:20:32 2021

@author: Kaouther HARBI
"""
import os
from tkinter import Entry, Label, StringVar, Tk, Button, END, messagebox
import pandas as pd
from individu import Individu


DICO_INDIVIDUALS = {}
def get_fields():
    """ Gets the information filled by the user """
    if check_all_filedds_filled()  is True:
        if WIDGETS_ENTRY["name"].get() \
        and WIDGETS_ENTRY["first_name"].get() not in DICO_INDIVIDUALS:
            user = Individu(WIDGETS_ENTRY["name"].get(),
                            WIDGETS_ENTRY["first_name"].get(),
                            WIDGETS_ENTRY["phone"].get(),
                            WIDGETS_ENTRY["adress"].get(),
                            WIDGETS_ENTRY["city"].get())
            DICO_INDIVIDUALS[WIDGETS_ENTRY["name"].get()] = user
        else:
            messagebox.showwarning(title='Warning', message="Already inserted")
    else:
        messagebox.showwarning(title='Error', message="Lazy! You need to fill all the fields")
    clear_fields()
    WIDGETS_ENTRY["name"].focus()
    return DICO_INDIVIDUALS
def export():
    """ Exports the phone book as a CSV file"""
    book = []
    for key in DICO_INDIVIDUALS:
        book.append([DICO_INDIVIDUALS[key].name, DICO_INDIVIDUALS[key].firstname,
                     DICO_INDIVIDUALS[key].phone, DICO_INDIVIDUALS[key].adress,
                     DICO_INDIVIDUALS[key].city])
    book_df = pd.DataFrame(book, columns=IDS)
    #print(book_df)
    current_location = str(os.getcwd())
    book_df.to_csv(current_location+'phone_book.CSV', index=False, header=True)
def clear_fields():
    """Remove text from all the fields"""
    for idi in IDS:
        WIDGETS_ENTRY[idi].delete(0, END)
    WIDGETS_ENTRY["name"].focus()
def check_all_filedds_filled():
    """check all fields are filled before insertion"""
    if  WIDGETS_ENTRY["name"].get() != "" \
    and WIDGETS_ENTRY["first_name"].get() != "" \
    and WIDGETS_ENTRY["phone"].get() != ""  \
    and WIDGETS_ENTRY["adress"].get() != "" \
    and WIDGETS_ENTRY["city"].get() != "":
        return True
    else:
        return False
def search():
    """ finds user's inforamtion by his/her name"""
    key_name = WIDGETS_ENTRY["name"].get()
    if key_name in DICO_INDIVIDUALS:
        sub_window = Tk()
        sub_window.title("Contact information")
        liste = [DICO_INDIVIDUALS[key_name].name, DICO_INDIVIDUALS[key_name].firstname,
                 DICO_INDIVIDUALS[key_name].phone, DICO_INDIVIDUALS[key_name].adress,
                 DICO_INDIVIDUALS[key_name].city]
        line = 1
        for idi in IDS:
            label = Label(sub_window, text=idi + ": ")
            label.grid(row=line, column=0)
            line += 1
        line = 1
        for info in liste:
            print(liste)
            information = Label(sub_window, text=info)
            information.grid(row=line, column=1)
            line += 1
    else:
        messagebox.showwarning(title='Error',
                               message="Inexistant contact")
    clear_fields()
ROOT = Tk()
ROOT.title('Phone book')

IDS = ["name", "first_name", "phone", "adress", "city"]
BOUTONS = ["chercher", "inserer", "sauvgarder", "effacer_champs"]

WIDGETS_LABS = {}
WIDGETS_ENTRY = {}
WIDGETS_BUTTON = {}
i, j = 0, 0
for idi in IDS:
    lab = Label(ROOT, text=idi)
    WIDGETS_LABS[idi] = lab
    lab.grid(row=i, column=0)
    var = StringVar()
    entry = Entry(ROOT, text=var)
    WIDGETS_ENTRY[idi] = entry
    entry.grid(row=i, column=1)
    i += 1
for idi in BOUTONS:
    button = Button(ROOT, text=idi)
    WIDGETS_BUTTON[idi] = button
    button.grid(row=i+1, column=j)
    j += 1
WIDGETS_BUTTON["inserer"].config(command=get_fields)
WIDGETS_BUTTON["effacer_champs"].config(command=clear_fields)
WIDGETS_BUTTON["chercher"].config(command=search)
WIDGETS_BUTTON["sauvgarder"].config(command=export)
ROOT.mainloop()
