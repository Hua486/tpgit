#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 08:20:32 2021

@author: Kaouther HARBI
"""

class Individu :
    def __init__(self,name, firstname,phone, adress, city):
        self.name = name
        self.firstname = firstname
        self.phone = phone
        self.adress = adress
        self.city = city
        

    def __str__(self):
        return (self.name +' '+ self.firstname +' '+ self.phone +' '+ self.adress +' '+ self.city)
    