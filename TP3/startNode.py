# coding: utf-8
"""
Created on Mon Jan 27 22:10:22 2021
@author: Kaouther Harbi
"""
from chainedList import ChainedList

if __name__ == "__main__":
    chained_list = ChainedList([1, 5, 6, 12, 34])
    print(chained_list.first_node)
    chained_list.insert_node_after(5, 8)
    print("Chained list after the insert")
    print(chained_list.first_node)
    chained_list.delete_node(5)
    print("Chained list after delete")
    print(chained_list.first_node)
