# coding: utf-8
"""
Created on Mon Jan 27 22:10:22 2021
@author: Kaouther Harbi
"""
class Node:
    """
    This class is made of method and attributes to make links between nodes
    ----------
    nodes : list
    """
    def __init__(self, param_data):
        """
        This is a constructor : method called each time an object
        (a node) is created
        ----------
        param_data : value given to the node
        """
        self.data = param_data
        self.link = None

    def __str__(self):
        """
        Methode to print the nodes
        returns
        ---------
        list : all nodes
        """
        liste = []
        node = self
        while node.link is not None:
            liste.append(node.data)
            node = node.link
        liste.append(node.data)
        return str(liste)
