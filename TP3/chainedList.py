# coding: utf-8
"""
Created on Mon Jan 27 22:10:22 2021
@author: Kaouther Harbi
"""
from node import Node
class ChainedList:
    """
    Chained list Object
    Parameters
    ----------
    nodes : list
        list that we want to transfert in a chained list of Node object
    """
    def __init__(self, nodes):
        """
        This function des initialize the first element of the list
        it does a conversion of a list into a chained list adding a link
        berween the nodes
        -----------
        nodes: list to convert to a chained list of Node
        """
        self.first_node = None
        #self.nodes= Node(nodes)
        i = 0
        initial = self.first_node
        while i < len(nodes):
            if i == 0:
                self.first_node = Node(nodes[i])
                initial = self.first_node
            else:
                initial.link = Node(nodes[i])
                initial = initial.link
            i += 1
    def insert_node_after(self, data, new_node):
        """
        insert a new node after the node with the value == data
        Parameters
        ----------
        data : target data
        new_node : node to insert
        """
        current_node = self.first_node
        while current_node.link is not None:
            current_node = current_node.link
            if current_node.data == data: #if its a match w insert the new node
                break
        new_node = Node(new_node)
        new_node.link = current_node.link #link created and the previous node
        current_node.link = new_node
    def delete_node(self, target):
        """
        delete all node(s) value == data
        Parameters
        ----------
        target : searched data to delete
        """
        current = self.first_node #point to the first element
          #if the target isn't in the list we  return the list
        if current is None:
            return
        while current is not None:
            if current.data == target: #search for target
                break
            before = current
            #the target element will be dismessed and we create a link
            #between the previous and next element of the chained list
            current = current.link
        before.link = current.link
